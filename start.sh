#! /bin/bash

if [ ! -f /config/mkdocs.yml ]
then
  cp /etc/default-mkdocs.yml /config/mkdocs.yml
fi


# get docs dir from config - for watching.
DOCS_DIR=`grep docs_dir: /config/mkdocs.yml | awk '{print $2}' | sed "s/'//g"`


# set default delay of 10 seconds
if [ -z "$BUILD_DELAY" ]
then
  BUILD_DELAY=10
fi

# build once, just to make sure.
echo "$(date --iso-8601=seconds): Initial Build"
/usr/local/bin/mkdocs build -f /config/mkdocs.yml --clean


# wait for changes, then build
echo "$(date --iso-8601=seconds): Monitoring directory $DOCS_DIR for changes."
/usr/bin/inotifywait -mr --timefmt '%d/%m/%y %H:%M' --format '%T %w %f' \
-e close_write $DOCS_DIR | while read date time dir file; do

  # wait a bit - more files may be syncing.
  echo "$(date --iso-8601=seconds): Changes detected.  Sleeping $BUILD_DELAY seconds before build."
  sleep ${BUILD_DELAY}

  # build the site
  /usr/local/bin/mkdocs build -f /config/mkdocs.yml --clean

done
