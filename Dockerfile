FROM python:3-slim

RUN apt-get update -qq && \
	apt-get -y --no-install-recommends install \
	inotify-tools \
	&& rm -rf /var/lib/apt/lists/*


RUN pip install --upgrade --no-cache-dir mkdocs mkdocs-bootswatch mkdocs-cinder mkdocs-material mkdocs-rtd-dropdown

VOLUME /config
VOLUME /docs
VOLUME /site

WORKDIR /config
COPY default-mkdocs.yml /etc/default-mkdocs.yml
COPY start.sh /usr/local/bin/start.sh

CMD /bin/bash /usr/local/bin/start.sh

