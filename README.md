# mkdocs-docker

Docker image to watch and rebuild site from markdown documents using mkdocs.
Docker Hub: [mkdocs-docker](https://hub.docker.com/r/rveach/mkdocs-docker/)

## Usage

This image relies on three volumes:

* `/config` - This is a persistent volume used to maintain a config file.  If config.yml is not present, one will be copied into the volume.  More on this later.
* `/docs` - This volume should contain the source markdown files.
* `/site` - This volume is where the site files will be written with the default config.

```bash
docker run --rm -it \
-v /home/myuser/docs:/docs:ro \
-v /var/www/html:/site \
mkdocs-docker
```

## Configs

The config file is located in `/config/mkdocs.yml`.

The `/config` volume can be used to store configs.  The default config file will be automatically copied to `/config/mkdocs.yml` if one does not exist.  The default config file is as follows:

```yaml
site_name: My Docs
theme: 'mkdocs'
use_directory_urls: false
docs_dir: '/docs'
site_dir: '/site'
```

If you would like to implement a custom config file, please reference the [MkDocs Documentation](http://www.mkdocs.org/) for more information on how to configure this file.

```bash
docker run --rm -it \
-v /home/myuser/docs:/docs:ro \
-v /var/www/html:/site \
-v home/myuser/docconfig:/config:ro \
mkdocs-docker
```

## Source Information

* Source Repo: [gitlab.com/rveach/mkdocs-docker](https://gitlab.com/rveach/mkdocs-docker)
* [Dockerfile](https://gitlab.com/rveach/mkdocs-docker/blob/master/Dockerfile)
* [Issues](https://gitlab.com/rveach/mkdocs-docker/issues)

#### Dependency Notes

* This Docker images is created from the official [Python 3.6 slim stretch image](https://hub.docker.com/_/python/).
* The debian package [inotify-tools](https://packages.debian.org/search?keywords=inotify-tools) is used to monitor the filesystem.
* The following python packages are installed for mkdocs site generation and theme. They are installed using the latest available version at image build time.
  * [mkdocs](https://pypi.org/project/mkdocs/)
  * [mkdocs-bootswatch](https://pypi.org/project/mkdocs-bootswatch/)
  * [mkdocs-cinder](https://pypi.org/project/mkdocs-cinder/)
  * [mkdocs-material](https://pypi.org/project/mkdocs-material/)
  * [mkdocs-rtd-dropdown](https://pypi.org/project/mkdocs-rtd-dropdown/)

